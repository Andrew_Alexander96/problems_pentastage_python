def func_3(name, birth_year):
    my_list = []
    for i in range(len(name)):
        my_list.append((name[i], birth_year[i]))
    return dict(sorted(my_list, key=lambda tup: int(tup[1])))


print(func_3(['Gabi', 'Andreea', 'Paul'], [1993, 1986, 1997]))
