import sys


def func_2(my_file, str_ch):
    file = open(my_file, "r")
    my_list = []
    lines = file.readlines()
    for line in lines:
        if(line.find(str_ch) != -1):
            my_list.append(line)
    return my_list


str_ch = sys.argv[2] + " " + sys.argv[3] + " " + sys.argv[4]
print(func_2(sys.argv[1], str_ch))
