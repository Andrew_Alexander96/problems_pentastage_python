from collections import defaultdict
phone_book = {}

def show_agenda(*args):    
    print(phone_book)


def add_contact(*args):
    phone_book[args] = ' '


def delete_contact(string_name):
    del phone_book[string_name]


ans = True
while ans:
    print("""
    *********** MENU ***********
    1 - Show agenda
    2 - Add contact
    3 - Add phone nr. to contact
    4 - Find contact
    5 - Delete contact
    0 - Exit
    ****************************
    """)
    ans = input("Your option: ")
    if ans == "1":
        print("\n Show agenda")
        show_agenda(phone_book)
    elif ans == "2":
        print("\n Add contact")
    elif ans == "3":
        print("\n Add phone nr. to contact")
    elif ans == "4":
        print("\n Find contact")
    elif ans == "5":
        print("\n Delete contact")
    elif ans == "0":
        break
