from collections import defaultdict
import json

phone_book = defaultdict(list)
def show_agenda():
    print(json.dumps(phone_book, indent=4))


def add_contact():
    name = input("Enter the name:")
    number = input("Enter the number:")
    try:
        phone_book[name].append(number)
    except:
        phone_book[name] = number


def find_contact():
    name = input("Enter the name:")
    print(json.dumps(phone_book[name], indent=4))


def delete_contact():
    name = input('Enter the name:')
    try:
        del phone_book[name]
    except:
        print("This contact aren't in the database:")


def wait_for_key():
    try:
        input("Press any key to continue...")
    except:
        pass


if __name__ == '__main__':
    phone_book = defaultdict(list)
    while True:
        print("""
        *********** MENU ***********
        1 - Show agenda
        2 - Add contact
        3 - Add phone nr. to contact
        4 - Find contact
        5 - Delete contact
        0 - Exit
        ****************************
        """)
        ans = str(input("Your option: "))
        if ans == '1':
            print("\n Show agenda")
            show_agenda()
            wait_for_key()
        elif ans == '2':
            print("\n Add contact")
            add_contact()
            wait_for_key()
        elif ans == '3':
            add_contact()
            wait_for_key()
        elif ans == '4':
            find_contact()
            wait_for_key()
        elif ans == '5':
            delete_contact()
            wait_for_key()
        elif ans == '0':
            break
        else:
            print("This is not a valid option!")
            wait_for_key()
